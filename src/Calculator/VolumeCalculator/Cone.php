<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 3/21/2018
 * Time: 2:19 PM
 */
namespace Pondit\Calculator\VolumeCalculator;


class Cone
{
    public $radius;
    public $pi;
    public $height;


    public function getCon()
    {
        return $this->pi * $this->radius * $this->radius * $this->height/3;
    }
}