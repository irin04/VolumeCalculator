<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 3/16/2018
 * Time: 12:37 PM
 */


include_once "vendor/autoload.php";

use Pondit\Calculator\VolumeCalculator\Cube;
use Pondit\Calculator\VolumeCalculator\Displayer;

$cube1 = new Cube();
$cube1->side_length = 10;




$displayer=new Displayer();
$displayer->displaypre($cube1->getSL());
$displayer->displayH1($cube1->getSL());
$displayer->displaysimple($cube1->getSL());

